"use client"
import React, { useState } from 'react'
import './App.css'
import Container from './Container';
import Score from './Score';
import Timer from './Timer'
import Controls from './Controls';
import { resolve } from 'styled-jsx/css';

function randIntToTab(size,c=2) {
  let tab_to_push=[];
  let final_tab=[];
  for (let i = 1; i <= size; i++) {
    let p;
    if (i<=(size/c)) p=i
    else {
      let m=1;
      for (let j = 1; j <= c; j++) {
        if (i>=(size/c)*(j-1) && i<=(size/c)*(j)) {
          m=j;
          break;
        }
      }
      p=i-((size/c)*(m-1))
    }
    tab_to_push.push(p)
  }

  for (let i = 0; i < size; i++) {
    const index=Math.floor(Math.random()*(tab_to_push.length));
    final_tab.push(tab_to_push[index]);
    tab_to_push.splice(index,1);
  }
  return final_tab;
}

function randElementFromTab(tab) {
  var el=tab[Math.floor(Math.random()*tab.length)];
  return el;
}

const wait=function(time=1000) {
  return new Promise(function(resolve) {
    setTimeout(resolve,time)
  })
}

class App extends React.Component{
  constructor(props){
    super(props);
    this.niv=2;
    let tab=this.setNiveauForRandIntToTab(this.niv)
    let tmp=[];
    let backIndex=randElementFromTab([1,2,3,4]);
    tab.forEach((element,index) => {
      tmp.push({
        ID:index,
        back:'/assets/back/back-'+backIndex+'.png',
        front:'/assets/img/img-'+tab[index]+'.png',
        show:false
      })
    });
    this.firstClicked=-1;
    this.secundClicked=-2;
    this.initialState={
      cases:tmp,
      firstClick:false,
      secundClick:false,
      canClick:true,
      visibilityTime:1500,
      score:0,
      time:0,
      gagne:false,
      niveau:this.niv,
      showGagneAlert:false,
      finalScore:0,
      finalTime:0
    };
    this.state=this.initialState;
}

setNiveauForRandIntToTab(n){
  let tab=[];
  if (n==1) {
    tab=randIntToTab(16);
  }else if (n==2) {
    tab=randIntToTab(64,8);
  }else if (n==3) {
    tab=randIntToTab(64,2);
  }
  return tab;
}

reinitialize(niveau=1){
    let tab=this.setNiveauForRandIntToTab(niveau);
    let tmp=[];
    let backIndex=randElementFromTab([1,2,3,4]);
    tab.forEach((element,index) => {
      tmp.push({
        ID:index,
        back:'/assets/back/back-'+backIndex+'.png',
        front:'/assets/img/img-'+tab[index]+'.png',
        show:true
      })
    });
    this.firstClicked=-1;
    this.secundClicked=-2;
    let tmpState={
      cases:tmp,
      firstClick:false,
      secundClick:false,
      canClick:true,
      visibilityTime:1500,
      score:0,
      time:0,
      gagne:false,
      niveau:this.niv,
    };
    this.setState((()=>tmpState)())
    //tout fermer
    setTimeout(() => {
      this.allVisible(false)
    }, this.state.visibilityTime);
}

  allVisible(visible){
    if (visible) {
      let tmp=this.state.cases;
      tmp.forEach(element => {
        element.show=true;
      });
      this.setState({cases:tmp});
    }else{
      let tmp=this.state.cases;
      tmp.forEach(element => {
        element.show=false;
      });
      this.setState({cases:tmp});
    }
  }

  setIsVisibleCase(ID,visible=true,time=1000){
    let tmp=this.state.cases;
    tmp[ID].show=visible;
    console.log('closed');
    this.setState({
      cases:tmp
    });
  }

  componentDidMount(){
    this.allVisible(true);
    setTimeout(() => {
      this.allVisible(false);
    }, this.state.visibilityTime);
    //Timer
    setInterval(() => {
      if(!this.state.gagne)this.setState({time:(()=>this.state.time+1)()})
    }, 1000);
  }

  verifierSiGagne(){
    let tmp=this.state.cases;
    let gagner=true;
    tmp.forEach(element => {
      if (!element.show) {
        gagner=false;
      }
    });
    this.setState({gagne:gagner});
    return gagner;
  }

  async handleClickOneCase(ID){
    if(this.state.canClick){
      if (this.state.firstClick && (ID==this.firstClicked)) {
        //fermer
        let tmp=this.state.cases;
        tmp[ID].show=false;
        this.setState({
          cases:tmp,
          firstClick:false
        });
      }
      else if (!this.state.firstClick) {
        if(!this.state.cases[ID].show){
          //premier click
          let tmp=this.state.cases;
          tmp[ID].show=true;
          this.setState({
            cases:tmp,
            firstClick:true
          });
          this.firstClicked=ID
        }
      }else if (this.state.firstClick && (!this.state.secundClick)) {
        if(!this.state.cases[ID].show){
          //second click
          this.canClick=false
          console.log("second");
          let tmp=this.state.cases;
          tmp[ID].show=true;
          this.setState({
            cases:tmp,
            secundClick:true
          });
          this.secundClicked=ID;
          let equal=(this.state.cases[this.firstClicked].front == this.state.cases[ID].front);
          console.log(equal);
          if (!equal) {
            await wait(600) 
            this.setIsVisibleCase(this.firstClicked,false);
            this.setIsVisibleCase(this.secundClicked,false);
          }else{
            //score ++
            this.setState({score:(()=>this.state.score+1)()})
            //verifierSiGagne
            if(this.verifierSiGagne()) {
              setTimeout(() => {
                //alert('GAGNE\nPaires trouvées:'+this.state.score+'\nTemps ecoulées:'+this.state.time)
                this.setState({finalScore:this.state.score,finalTime:this.state.time,showGagneAlert:true})
              }, 500)
            }
          }
          this.setState({
            firstClick:false,
            secundClick:false,
            canClick:true
          })
          console.log("fin");
        }
      }
    }else{
      console.log("CAN'T CLICK!!");
    }
  }

  handleChangeNiveau(n){
    this.reinitialize(n);
    this.setState({niveau:n});
  }

  handleReboot(){
    let niv=this.state.niveau;
    this.reinitialize(niv);
    this.setState({niveau:niv});
    this.setState({niveau:niv,showGagneAlert:false});
  }

  render(){
    return<>
      {
        this.state.showGagneAlert?
        
        <div className="__gagne_alert_overlay__">
            <div className="__g_alert__">
              <h2>Vous avez gagné</h2>
              <p>Couple de cases trouvées : {this.state.finalScore} </p>
              <p>Temps écoulés: {this.state.finalTime} s </p>
              <div className="__btnss__">
                <button onClick={this.handleReboot.bind(this)}>Rejouer</button>
              </div>
            </div>
        </div>
        :
        <>
          <div className="etats">
            <Score value={this.state.score}></Score>
            <Timer value={this.state.time}></Timer>
          </div>
          <Container niveau={this.state.niveau} onClickOne={this.handleClickOneCase.bind(this)} setCases={this.state.cases}></Container>
          <Controls niveau={this.state.niveau} onReboot={this.handleReboot.bind(this)} onChangeNiv={this.handleChangeNiveau.bind(this)}></Controls>      
        </>
      }

    </>
  }
}

export default App
